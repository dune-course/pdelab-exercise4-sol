// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef LDOM_EXAMPLE_ESTIMATOR_HH
#define LDOM_EXAMPLE_ESTIMATOR_HH


/** a local operator for residual-based error estimation
 *
 * A call to residual() of a grid operator space will assemble
 * the quantity \eta_T^2 for each cell. Note that the squares
 * of the cell indicator \eta_T is stored. To compute the global
 * error estimate sum up all values and take the square root.
 *
 * Assumptions and limitations:
 * - Assumes that LFSU is P_1/Q_1 finite element space
 *   and LFSV is a P_0 finite element space (one value per cell).
 *
 * \tparam PROBLEM model of DiffusionParameterInterface
 */
template<typename PROBLEM>
class LDomainExampleResidualEstimator
  : public Dune::PDELab::LocalOperatorDefaultFlags
{
private:

  typedef typename PROBLEM::Traits::RangeFieldType Real;

  PROBLEM& param;  // problem parameter class

  template<class GEO>
  typename GEO::ctype diameter (const GEO& geo) const
  {
    typedef typename GEO::ctype DF;
    DF hmax = -1.0E00;
    const int dim = GEO::coorddimension;
    for (int i=0; i<geo.corners(); i++)
      {
        Dune::FieldVector<DF,dim> xi = geo.corner(i);
        for (int j=i+1; j<geo.corners(); j++)
          {
            Dune::FieldVector<DF,dim> xj = geo.corner(j);
            xj -= xi;
            hmax = std::max(hmax,xj.two_norm());
          }
      }
    return hmax;
  }

public:

  // pattern assembly flags
  enum { doPatternVolume = false };
  enum { doPatternSkeleton = false };

  // residual assembly flags
  enum { doAlphaVolume  = true };
  enum { doAlphaSkeleton  = true };
  enum { doAlphaBoundary  = true };

  //! constructor: pass parameter object
  LDomainExampleResidualEstimator (PROBLEM& param_)
    : param(param_)
  {}

  // volume integral depending on test and ansatz functions
  template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x,
                     const LFSV& lfsv, R& r) const
  {
    // domain and range field type
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSU::Traits::SizeType size_type;

    // dimensions
    const int dim = EG::Geometry::dimension;
    const int intorder = 2;

    // select quadrature rule
    Dune::GeometryType gt = eg.geometry().type();
    const Dune::QuadratureRule<DF,dim>& rule =
      Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

    // loop over quadrature points
    RF sum(0.0);
    for (const auto& ip : rule)
    {
      // evaluate basis functions
      std::vector<RangeType> phi(lfsu.size());
      lfsu.finiteElement().localBasis().evaluateFunction(ip.position(),phi);

      // evaluate u
      RF u=0.0;
      for (size_type i=0; i<lfsu.size(); i++) u += x(lfsu, i)*phi[i];

      // evaluate reaction term
      Real c = param.c(eg.entity(),ip.position());

      // evaluate right hand side parameter function
      Real f = param.f(eg.entity(),ip.position());

      // integrate (f - c*u_h)^2
      RF factor = ip.weight()
                    * eg.geometry().integrationElement(ip.position());
      sum += (f-c*u)*(f-c*u)*factor;
    }

    // accumulate cell indicator
    DF h_T = diameter(eg.geometry());
    r.accumulate(lfsv, 0, h_T*h_T*sum);
  }


  // skeleton integral depending on test and ansatz functions
  // each face is only visited ONCE!
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_skeleton (const IG& ig,
                       const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                       const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                       R& r_s, R& r_n) const
  {
    // domain and range field type
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    //typedef typename LFSU::Traits::FiniteElementType::
    //  Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU::Traits::SizeType size_type;

    // dimensions
    const int dim = IG::dimension;

    // select quadrature rule
    const int intorder = 2;
    Dune::GeometryType gtface = ig.geometryInInside().type();
    const Dune::QuadratureRule<DF,dim-1>& rule =
      Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

    // transformation
    Dune::FieldMatrix<DF,dim,dim> jac;

    // normal vector
    const Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();

    // loop over quadrature points and integrate normal flux
    RF sum(0.0);
    for (const auto& ip : rule)
    {
      // TODO: Insert your code here:
      // <begin-insert>

      // position of quad. point in local coordinates of internal element:
      Dune::FieldVector<DF,dim> iplocal_s
        = ig.geometryInInside().global(ip.position());

      // position of quadr. point in local coordinates of external element:
      Dune::FieldVector<DF,dim> iplocal_n
        = ig.geometryInOutside().global(ip.position());

      // evaluate gradient of basis functions for the internal element:
      std::vector<JacobianType> gradphi_s(lfsu_s.size());
      lfsu_s.finiteElement().localBasis().evaluateJacobian(iplocal_s,gradphi_s);

      // evaluate gradient of basis functions for the external element:
      std::vector<JacobianType> gradphi_n(lfsu_n.size());
      lfsu_n.finiteElement().localBasis().evaluateJacobian(iplocal_n,gradphi_n);

      // transform gradients of shape functions to real element for the internal element:
      jac = ig.inside().geometry().jacobianInverseTransposed(iplocal_s);
      std::vector<Dune::FieldVector<RF,dim> > tgradphi_s(lfsu_s.size());
      for (size_type i=0; i<lfsu_s.size(); i++)
        jac.mv(gradphi_s[i][0],tgradphi_s[i]);

      // transform gradients of shape functions to real element for the external element:
      jac = ig.outside().geometry().jacobianInverseTransposed(iplocal_n);
      std::vector<Dune::FieldVector<RF,dim> > tgradphi_n(lfsu_n.size());
      for (size_type i=0; i<lfsu_n.size(); i++)
        jac.mv(gradphi_n[i][0],tgradphi_n[i]);

      // compute gradient of u for the internal element:
      Dune::FieldVector<RF,dim> gradu_s(0.0);
      for (size_type i=0; i<lfsu_s.size(); i++)
        gradu_s.axpy(x_s(lfsu_s, i), tgradphi_s[i]);

      // compute gradient of u for the internal element:
      Dune::FieldVector<RF,dim> gradu_n(0.0);
      for (size_type i=0; i<lfsu_n.size(); i++)
        gradu_n.axpy(x_n(lfsu_n, i), tgradphi_n[i]);

      // compute integration factor:
      RF factor = ip.weight() * ig.geometry().integrationElement(ip.position());

      // compute jump:
      RF jump = (n_F*gradu_s)-(n_F*gradu_n);

      // sum up jump (L^2-norm):
      sum += jump*jump*factor;

      // <end-insert>
    }

    // accumulate indicator
    DF h_T = diameter(ig.geometry());
    r_s.accumulate(lfsv_s, 0, 0.5*h_T*sum);
    r_n.accumulate(lfsv_n, 0, 0.5*h_T*sum);
  }


  // boundary integral depending on test and ansatz functions
  // We put the Dirchlet evaluation also in the alpha term to save
  // some geometry evaluations
  template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
  void alpha_boundary (const IG& ig,
                       const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                       R& r_s) const
  {
    // domain and range field type
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::DomainFieldType DF;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::RangeFieldType RF;
    //typedef typename LFSU::Traits::FiniteElementType::
    //  Traits::LocalBasisType::Traits::RangeType RangeType;
    typedef typename LFSU::Traits::FiniteElementType::
      Traits::LocalBasisType::Traits::JacobianType JacobianType;
    typedef typename LFSU::Traits::SizeType size_type;

    // dimensions
    const int dim = IG::dimension;

    // normal vector
    const Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();

    // select quadrature rule
    const int intorder = 2;
    Dune::GeometryType gtface = ig.geometryInInside().type();
    const Dune::QuadratureRule<DF,dim-1>& rule =
      Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

    // transformation
    Dune::FieldMatrix<DF,dim,dim> jac;

    // skip rest if we are on Dirichlet boundary
    const Dune::FieldVector<DF,dim-1> face_local =
      Dune::ReferenceElements<DF,dim-1>::general(gtface).position(0,0);
    DiffusionBoundaryConditionAdapter<PROBLEM> bctype(param);
    if (bctype.isDirichlet(ig.intersection(),face_local));
      return;

    // loop over quadrature points and integrate normal flux
    RF sum(0.0);
    for (const auto& ip : rule)
    {
      // position of quadrature point in local coordinates of elements
      Dune::FieldVector<DF,dim> iplocal_s =
        ig.geometryInInside().global(ip.position());

      // evaluate gradient of basis functions
      std::vector<JacobianType> gradphi_s(lfsu_s.size());
      lfsu_s.finiteElement().localBasis().evaluateJacobian(iplocal_s,gradphi_s);

      // transform gradients of shape functions to real element
      jac = ig.inside().geometry().jacobianInverseTransposed(iplocal_s);
      std::vector<Dune::FieldVector<RF,dim> > tgradphi_s(lfsu_s.size());
      for (size_type i=0; i<lfsu_s.size(); i++)
        jac.mv(gradphi_s[i][0],tgradphi_s[i]);

      // compute gradient of u
      Dune::FieldVector<RF,dim> gradu_s(0.0);
      for (size_type i=0; i<lfsu_s.size(); i++)
        gradu_s.axpy(x_s(lfsu_s,i), tgradphi_s[i]);

      // evaluate flux boundary condition
      RF j = param.j(ig.intersection(),ip.position());

      // integrate
      RF factor = ip.weight()
                    * ig.geometry().integrationElement(ip.position());
      RF jump = j+(n_F*gradu_s);
      sum += jump*jump*factor;
    }

    // accumulate indicator
    DF h_T = diameter(ig.geometry());
    r_s.accumulate(lfsv_s, 0, h_T*sum);
  }
};

#endif // LDOM_EXAMPLE_ESTIMATOR_HH
