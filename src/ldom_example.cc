// -*- tab-width: 4; indent-tabs-mode: nil -*-
/** \file

    \brief Solve elliptic problem in constrained spaces with
           conforming finite elements
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include<math.h>
#include<iostream>
#include<vector>
#include<map>
#include<string>

#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/timer.hh>
#include<dune/common/parametertree.hh>
#include<dune/common/parametertreeparser.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/grid/io/file/gmshreader.hh>
#include<dune/grid/yaspgrid.hh>

#if HAVE_ALBERTA
#include<dune/grid/albertagrid.hh>
#include <dune/grid/albertagrid/dgfparser.hh>
#endif

#if HAVE_UG
#include<dune/grid/uggrid.hh>
#endif

#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/superlu.hh>

#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/finiteelementmap/qkfem.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/backend/istl.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>

#include<dune/pdelab/adaptivity/adaptivity.hh>
#include<dune/pdelab/common/functionutilities.hh>


#include"ldom_example_functionadapters.hh"
#include"ldom_example_structs.hh"
#include"ldom_example_parameters.hh"
#include"ldom_example_operator.hh"
#include"ldom_example_estimator.hh"
#include"ldom_example_grid.hh"
#include"ldom_example_P1.hh"

//===============================================================
// Main program with grid setup
//===============================================================

int main(int argc, char** argv)
{
  try{
    // Maybe initialize Mpi
    Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
    if (Dune::MPIHelper::isFake)
      std::cout<< "This is a sequential program." << std::endl;
    else
    {
      if(helper.rank()==0)
        std::cout << "parallel run on " << helper.size() << " process(es)" << std::endl;
    }

    if (argc!=2)
    {
      if (helper.rank()==0)
        std::cout << "usage: ./ldom_example <cfg-file>" << std::endl;
      return 1;
    }

    // Parse configuration file.
    std::string config_file(argv[1]);
    Dune::ParameterTree configuration;
    Dune::ParameterTreeParser parser;

    try{
      parser.readINITree( config_file, configuration );
    }
    catch(...){
      std::cerr << "Could not read config file \""
                << config_file << "\"!" << std::endl;
      exit(1);
    }

    std::string mesh = configuration.get<std::string>("ldomain.mesh");
    int startlevel = configuration.get<int>("ldomain.startlevel");
    int maxsteps = configuration.get<int>("ldomain.maxsteps");
    double TOL = configuration.get<double>("ldomain.TOL");
    double fraction = configuration.get<double>("ldomain.fraction");
    int strategy = configuration.get<int>("ldomain.strategy");
    int verbose = configuration.get<int>("ldomain.verbose");

    if( mesh == "alberta" ){
#if HAVE_ALBERTA
      // Use Alberta grid
      typedef Dune::AlbertaGrid<2,2> GridType;
      GridType grid("ldomain.al");
      grid.globalRefine(startlevel);
      // Start simulation
      ldom_example_P1( grid, maxsteps, TOL, fraction, strategy, verbose, mesh );
#else
      std::cerr << "Alberta grid not found!" << std::endl;
#endif
    }

    if( mesh == "ugfactory" ){
#if HAVE_UG
      // Use UG grid factory
      typedef UGLDomain::Grid GridType;
      UGLDomain gridp;
      GridType &grid = gridp;
      grid.globalRefine(startlevel);
      // Start simulation
      ldom_example_P1( grid, maxsteps, TOL, fraction, strategy, verbose, mesh );
#else
      std::cerr << "UG grid not found!" << std::endl;
#endif
    }

    if( mesh == "uggmsh" ){
#if HAVE_UG
      // Make UG grid from Gmsh file

      typedef Dune::UGGrid<2> GridType;
      GridType grid;
      Dune::GridFactory<GridType> factory(&grid);
      Dune::GmshReader<GridType>::read(factory,"ldomain.msh");
      factory.createGrid();
      grid.globalRefine(startlevel);
      // Start simulation
      ldom_example_P1( grid, maxsteps, TOL, fraction, strategy, verbose, mesh );

#else
      std::cerr << "UG grid not found!" << std::endl;
#endif
    }

  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }
}
