// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef LDOM_EXAMPLE_GRID_HH
#define LDOM_EXAMPLE_GRID_HH

#if HAVE_UG

struct UGSetHeapSize
{
  UGSetHeapSize (unsigned int heapSize)
  {
    Dune::UGGrid<2>::setDefaultHeapSize(heapSize);
  }
};

class UGLDomain : UGSetHeapSize, public Dune::UGGrid<2>
{
public:

  typedef Dune::UGGrid<2> Grid;

  UGLDomain (unsigned int heapSize=100) : UGSetHeapSize(heapSize), Dune::UGGrid<2>()
  {
    Dune::GridFactory<Dune::UGGrid<2> > factory(this);
    Dune::FieldVector<double,2> pos;
    pos[0] =-1.0;  pos[1] =-1.0; factory.insertVertex(pos);
    pos[0] = 0.0;  pos[1] =-1.0; factory.insertVertex(pos);
    pos[0] =-1.0;  pos[1] = 0.0; factory.insertVertex(pos);
    pos[0] = 0.0;  pos[1] = 0.0; factory.insertVertex(pos);
    pos[0] = 1.0;  pos[1] = 0.0; factory.insertVertex(pos);
    pos[0] =-1.0;  pos[1] = 1.0; factory.insertVertex(pos);
    pos[0] = 0.0;  pos[1] = 1.0; factory.insertVertex(pos);
    pos[0] = 1.0;  pos[1] = 1.0; factory.insertVertex(pos);
    std::vector<unsigned int> cornerIDs(3);
    cornerIDs[0] = 0;  cornerIDs[1] = 1;  cornerIDs[2] = 2;
    factory.insertElement(Dune::GeometryType(Dune::GeometryType::simplex,2), cornerIDs);
    cornerIDs[0] = 2;  cornerIDs[1] = 1;  cornerIDs[2] = 3;
    factory.insertElement(Dune::GeometryType(Dune::GeometryType::simplex,2), cornerIDs);
    cornerIDs[0] = 2;  cornerIDs[1] = 3;  cornerIDs[2] = 5;
    factory.insertElement(Dune::GeometryType(Dune::GeometryType::simplex,2), cornerIDs);
    cornerIDs[0] = 5;  cornerIDs[1] = 3;  cornerIDs[2] = 6;
    factory.insertElement(Dune::GeometryType(Dune::GeometryType::simplex,2), cornerIDs);
    cornerIDs[0] = 3;  cornerIDs[1] = 4;  cornerIDs[2] = 6;
    factory.insertElement(Dune::GeometryType(Dune::GeometryType::simplex,2), cornerIDs);
    cornerIDs[0] = 6;  cornerIDs[1] = 4;  cornerIDs[2] = 7;
    factory.insertElement(Dune::GeometryType(Dune::GeometryType::simplex,2), cornerIDs);
    factory.createGrid();
  }
};

#endif

#endif // LDOM_EXAMPLE_GRID_HH
