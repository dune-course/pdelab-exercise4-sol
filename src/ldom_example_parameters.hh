// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef LDOM_EXAMPLE_PARAMETERS_HH
#define LDOM_EXAMPLE_PARAMETERS_HH

/** \brief Defines problem parameter: Dirichlet boundary conditions AND its
 *         extension to the interior, sinks and sources and the reaction rate.
 */
template<typename GV, typename RF>
class LDomainExampleParameters
{
public:

  typedef DiffusionParameterTraits<GV,RF> Traits;

  //! Dirichlet boundary condition type function
  template<typename I>
  inline bool isDirichlet(const I& intersection, const Dune::FieldVector<typename I::ctype, I::dimension-1>& xlocal) const
  {
    // Dirichlet b.c. on all the boundaries
    return true;
  }

  //! Neumann boundary condition type function
  template<typename I>
  inline bool isNeumann(const I& intersection, const Dune::FieldVector<typename I::ctype, I::dimension-1>& xlocal) const
  {
    // Dirichlet b.c. on all the boundaries
    return false;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    typename Traits::DomainFieldType theta = std::atan2(x[1], x[0]);
    if(theta < 0.0) theta += 2*M_PI;
    typename Traits::DomainFieldType r = x.two_norm();

    return pow(r,2.0/3.0)*std::sin(theta*2.0/3.0);
  }

  //! Neumann boundary condition value
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! reaction term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }
};

#endif // LDOM_EXAMPLE_PARAMETERS_HH
